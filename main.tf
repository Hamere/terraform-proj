#
# DO NOT DELETE THESE LINES UNTIL INSTRUCTED TO!
#
# Your AMI ID is:
#
#     ami-0fcfd45b96222a2ae
#
# Your subnet ID is:
#
#     subnet-0c338e4b9a903deaa
#
# Your VPC security group ID is:
#
#     sg-01989f120ea70a092
#
# Your Identity is:
#
#     terraform-training-buffalo
#
# Remove the entire block:
#
# resource "aws_instance" "web" {
# ...
# }

variable "access_key" {}
variable "secret_key" {}
variable "region" {
  default = "<REGION>"
}
#variable "ami" {}
variable "subnet_id" {}
variable "identity" {}
variable "vpc_security_group_ids" {
  type = list
}

module "server" {
  source = "tfe.couchtocloud.com/FAA/hamiserver/aws"
  #ami                    = var.ami
  subnet_id              = var.subnet_id
  vpc_security_group_ids = var.vpc_security_group_ids
  identity               = var.identity
  key_name               = module.keypair.key_name
  private_key            = module.keypair.private_key_pem
}

module "keypair" {
  source  = "mitchellh/dynamic-keys/aws"
  version = "2.0.0"
  path    = "${path.root}/keys"
  name    = "${var.identity}-key"
}

provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region     = var.region
}


output "public_ip" {
  value = module.server.public_ip
}

output "public_dns" {
  value = module.server.public_dns
}